// kursach.cpp: ���������� ����� ����� ��� ����������� ����������.
//
#define _CRT_SECURE_NO_WARNINGS
#include "stdafx.h"
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <Windows.h>

using namespace std;

const int	maxr = 100,	//������������ ���������� ���������
maxlevel = 5,	//������������ ���������� �������
maxcase = 1000, //������������ ���������� ������������ ��� ������� ��� ������
c = 1;	//��� ������

struct reader{
	char	number[10];	//����� ������������� ������
	char	FIO[150];	//���
	int		year;		//��� ��������
	char	adress[255];//�����
	char	place[255];	//����� ������/�����
};

struct book{
	char	code[9];		//����
	char	authors[255];	//�����(�)
	char	name[255];		//��������
	char	publisher[255];	//������������
	int		year;			//��� �������
	int		count;			//���������� �����������
	int		available;		//���������� � �������
	long	key(void){
		char temp[6];
		//����� �������
		temp[0] = code[0];
		temp[1] = code[1];
		temp[2] = code[2];
		//���������� ����� �����
		temp[3] = code[4];
		temp[4] = code[5];
		temp[5] = code[6];
		return atol(temp);
	}
};

struct inout{
	char	number[10];	//����� ������������� ������
	char	code[9];	//����
	char	date1[12];	//���� ������
	char	date2[12];	//���� ��������
};

//��������� ��� ���-������
struct node{
	long			key;	//���� ����
	book			data;	//������ � �����
	unsigned char	height;	//������ ��������� � ������ � ������ ����
	node			*left;	//��������� �� ����� ���������
	node			*right;	//���������	�� ������ ���������
};

//��������� ��� ������� ������
struct Node {
	inout data;
	Node *next;
};

reader	HashTable[maxr];	//��� �������
bool	HashTableUsed[maxr];//���� ��������� ����� � ����������
node	*ABLTreeRoot;		//������ ���-������
Node	*skiplist;			//������ ������
Node	*last;

//������� ������������� ����������� � ���������� ������� ��������
void Rus_1251(char* in, char* out){
	int len = strlen(in);
	for (int i = 0; i < len; i++){
		unsigned char simb = (*in);
		if ((simb >= 128) && (simb <= 175))
			simb += 64;
		else
			if ((simb >= 224) && (simb <= 239))
				simb += 16;
		*out = simb;
		out++;
		in++;
	}
}

//�������� ������� ������ ��������� � ������ - 1 ���� ��� ����� ���������,
//����� ������ ������� � ������� ���������� ���������
int	DirectSearch(char *string, char *substring){
	int sl, ssl;
	int res = -1;
	sl = strlen(string);
	ssl = strlen(substring);
	if (sl == 0)
		cout << "������� ������ ������\n";
	else if (ssl == 0)
		cout << "������� ������ ���������\n";
	else
		for (int i = 0; i < sl - ssl + 1; i++)
			for (int j = 0; j < ssl; j++)
				if (substring[j] != string[i + j])
					break;
				else if (j == ssl - 1){
					res = i;
					break;
				}
				return res;
}

//������� ��������� �������� reader
//struct reader{
//	char	number[8];	//����� ������������� ������
//	char	FIO[150];	//���
//	int		year;		//��� ��������
//	char	adress[255];//�����
//	char	place[255];	//����� ������/�����
//};
bool readerCmp(reader A, reader B){
	if (strcmp(A.number, B.number) == 0 &&
		strcmp(A.FIO, B.FIO) == 0 &&
		strcmp(A.adress, B.adress) == 0 &&
		strcmp(A.place, B.place) == 0 &&
		A.year == B.year)
		return true;
	else
		return false;
}

//��� �������
int myhash(char key[]){
	long int n = (int)key[0] + 2 * (int)key[1] + 4 * (int)key[2] + 8 * (int)key[3] + 16 * (int)key[4] +
		32 * (int)key[5] + 64 * (int)key[6] + 128 * (int)key[7];
	return (((n*n) / 100) % 10000) % maxr;
}

//������� ���������� ������ � ���-�������
bool insertDataToHash(reader data){
	int bucket, i = 0;
	bucket = myhash(data.number);
	while (HashTableUsed[bucket] && i < maxcase){
		bucket = (bucket + c * i) % maxr;
		i++;
	}
	if (!HashTableUsed[bucket] && i < maxcase){
		HashTableUsed[bucket] = true;
		HashTable[bucket] = data;
		return true;
	}
	else return false;
}

//������� ������ � ���-�������
bool findDataInHash(reader data){
	int bucket, i = 0;
	bucket = myhash(data.number);
	while (HashTableUsed[bucket] && i < maxcase && readerCmp(HashTable[bucket], data)){
		bucket = (bucket + c * i) % maxr;
		i++;
	}
	return HashTableUsed[bucket] && readerCmp(HashTable[bucket], data);
}

//������� �������� ������ �� ���-�������
bool deleteDataFromHash(reader data){
	int bucket, i = 0;
	bucket = myhash(data.number);
	while (HashTableUsed[bucket] && i < maxcase){
		if (HashTableUsed[bucket] && strcmp(HashTable[bucket].number, data.number) == 0){
			HashTableUsed[bucket] = false;
			return true;
		}
		bucket = (bucket + c * i) % maxr;
		i++;
	}
	return false;
}

//������� �������� ���� ������ �� ���-�������
void clearHashTable(void){
	for (int i = 0; i < maxr; i++)
		HashTableUsed[i] = false;
}

//������� ����������� ���-�������
void showHashTable(void){
	for (int i = 0; i < maxr; i++)
		if (HashTableUsed[i]){
		cout << "������: " << i << endl;
		cout << "\t���\t\t" << HashTable[i].number << endl;
		cout << "\t���\t\t" << HashTable[i].FIO << endl;
		cout << "\t�����\t\t" << HashTable[i].adress << endl;
		cout << "\t��� ����.\t" << HashTable[i].year << endl;
		cout << "\t����� �/�\t" << HashTable[i].place << endl;
		}
	system("PAUSE");
}

bool isNumber(char Symbol){ //�������� ������� �� �����
	return (Symbol >= '0' && Symbol <= '9') ? (false) : (true);
}
bool isAccess(char Symbol){  //������������ �� ����������
	return (Symbol == '�' || Symbol == '�' || Symbol == '�') ? (false) : (true);
}
bool isDash(char Symbol){  //������������ �� ����������
	return (Symbol == '-') ? (false) : (true);
}
bool isDot(char Symbol){  //������������ �� ����������
	return (Symbol == '.') ? (false) : (true);
}

//������ ��������� reader �� ������������ ������
reader readerInput(void){
	reader tempReader;
	cout << "������� ����� ������������� ������: ";
	do{
		cin.getline(tempReader.number, sizeof(tempReader.number));
		cout << endl;
		Rus_1251(tempReader.number, tempReader.number);
	} while (isAccess(tempReader.number[0]) || isNumber(tempReader.number[1]) ||
		isNumber(tempReader.number[2]) || isNumber(tempReader.number[3]) ||
		isNumber(tempReader.number[4]) || isDash(tempReader.number[5]) ||
		isNumber(tempReader.number[6]) || isNumber(tempReader.number[7]));
	cout << "������� ���: ";
	cin.getline(tempReader.FIO, sizeof(tempReader.FIO));
	Rus_1251(tempReader.FIO, tempReader.FIO);
	cout << endl;
	cout << "������� �����: ";
	cin.getline(tempReader.adress, sizeof(tempReader.adress));
	Rus_1251(tempReader.adress, tempReader.adress);
	cout << endl;
	cout << "������� ����� �����/������: ";
	cin.getline(tempReader.place, sizeof(tempReader.place));
	Rus_1251(tempReader.place, tempReader.place);
	cout << endl;
	cout << "������� ��� ��������: ";
	cin >> tempReader.year;
	cout << endl;
	return tempReader;
}

//������� ��� ���� height, ����� �������� � � �������� ����������� (� ������� ���������)
unsigned char height(node* p){
	return p ? p->height : 0;
}

//��������� balance factor ��������� ���� (� �������� ������ � ���������� �����������)
int bfactor(node* p){
	return height(p->right) - height(p->left);
}

//��������������� ���������� �������� ���� height ��������� ���� (��� �������, 
//��� �������� ����� ���� � ������ � ����� �������� ����� �������� �����������)
void fixheight(node* p){
	unsigned char hl = height(p->left);
	unsigned char hr = height(p->right);
	p->height = (hl>hr ? hl : hr) + 1;
}

//������ ������� ������ p
node* rotateright(node* p){
	node* q = p->left;
	p->left = q->right;
	q->right = p;
	fixheight(p);
	fixheight(q);
	return q;
}

//����� ������� ������ q
node* rotateleft(node* q)
{
	node* p = q->right;
	q->right = p->left;
	p->left = q;
	fixheight(q);
	fixheight(p);
	return p;
}

//������������ ���� p
node* balance(node* p){
	fixheight(p);
	if (bfactor(p) == 2)
	{
		if (bfactor(p->right) < 0)
			p->right = rotateright(p->right);
		return rotateleft(p);
	}
	if (bfactor(p) == -2)
	{
		if (bfactor(p->left) > 0)
			p->left = rotateleft(p->left);
		return rotateright(p);
	}
	return p; // ������������ �� �����
}

//������� ����� k � ������ � ������ p
node* insert(node* p, book k){
	if (p == NULL){
		p = new node;
		p->key = k.key();
		p->left = p->right = NULL;
		p->height = 1;
		strcpy(p->data.authors, k.authors);
		p->data.available = k.available;
		strcpy(p->data.code, k.code);
		p->data.count = k.count;
		strcpy(p->data.name, k.name);
		strcpy(p->data.publisher, k.publisher);
		p->data.year = k.year;
		return p;
	}
	if (k.key()<p->key)
		p->left = insert(p->left, k);
	else
		p->right = insert(p->right, k);
	return balance(p);
}

//����� ���� � ����������� ������ � ������ p 
node* findmin(node* p){
	return p->left ? findmin(p->left) : p;
}

//�������� ���� � ����������� ������ �� ������ p
node* removemin(node* p){
	if (p->left == 0)
		return p->right;
	p->left = removemin(p->left);
	return balance(p);
}

//�������� ����� k �� ������ p
node* remove(node* p, book k)
{
	if (!p) return 0;
	if (k.key() < p->key)
		p->left = remove(p->left, k);
	else if (k.key() > p->key)
		p->right = remove(p->right, k);
	else //  k == p->key 
	{
		node* q = p->left;
		node* r = p->right;
		delete p;
		if (!r) return q;
		node* min = findmin(r);
		min->right = removemin(r);
		min->left = q;
		return balance(min);
	}
	return balance(p);
}

//����� ������
void preorder(node *p){
	if (p != NULL){
		cout << "\t���� \t\t\t" << p->data.code << endl;
		cout << "\t������: \t\t" << p->data.authors << endl;
		cout << "\t��������: \t\t" << p->data.name << endl;
		cout << "\t������������: \t\t" << p->data.publisher << endl;
		cout << "\t��� �������:\t\t" << p->data.year << endl;
		cout << "\t����������:\t\t" << p->data.count << endl;
		cout << "\t�������:\t\t" << p->data.available << endl;
		preorder(p->left);
		preorder(p->right);
	}
}

//������� ����� ������
void emptyABLTree(node* &p){
	node* d;
	if (p != NULL)
	{
		emptyABLTree(p->left);
		emptyABLTree(p->right);
		d = p;
		delete d;
		p = NULL;
	}
}

//������ ��������� book �� ������������ ������
book bookInput(void){
	book tempBook;
	cout << "������� ���� �����: ";
	do{
		cin.getline(tempBook.code, sizeof(tempBook.code));
		cout << endl;
		Rus_1251(tempBook.code, tempBook.code);
	} while (isNumber(tempBook.code[0]) || isNumber(tempBook.code[1]) ||
		isNumber(tempBook.code[2]) || isDot(tempBook.code[3]) ||
		isNumber(tempBook.code[4]) || isNumber(tempBook.code[5]) || isNumber(tempBook.code[6]));
	cout << "������� �������: ";
	cin.getline(tempBook.authors, sizeof(tempBook.authors));
	Rus_1251(tempBook.authors, tempBook.authors);
	cout << endl;
	cout << "������� ��������: ";
	cin.getline(tempBook.name, sizeof(tempBook.name));
	Rus_1251(tempBook.name, tempBook.name);
	cout << endl;
	cout << "������� ������������: ";
	cin.getline(tempBook.publisher, sizeof(tempBook.publisher));
	Rus_1251(tempBook.publisher, tempBook.publisher);
	cout << endl;
	cout << "������� ��� �������: ";
	cin >> tempBook.year;
	cout << endl;
	cout << "������� ����� ���������� �����������: ";
	cin >> tempBook.count;
	cout << endl;
	cout << "������� ���������� ����������� � �������: ";
	cin >> tempBook.available;
	cout << endl;
	return tempBook;
}

//����� �������� �� �������
void searchReaderFIO(void){
	char temp[150];
	cout << "������� ���: " << endl;
	cin.clear();
	cin.sync();
	cin.getline(temp, sizeof(temp));
	Rus_1251(temp, temp);
	cout << temp;
	for (int i = 0; i < maxr; i++)
		if (HashTableUsed[i] && DirectSearch(HashTable[i].FIO, temp) > -1){
		cout << "������: " << i << endl;
		cout << "\t���\t\t" << HashTable[i].number << endl;
		cout << "\t���\t\t" << HashTable[i].FIO << endl;
		cout << "\t�����\t\t" << HashTable[i].adress << endl;
		cout << "\t��� ����.\t" << HashTable[i].year << endl;
		cout << "\t����� �/�\t" << HashTable[i].place << endl;
		}
	system("PAUSE");
}

//����� ����� �� ������� ������
void searchBookFIO(node *p, char FIO[255]){
	if (p != NULL){
		if (DirectSearch(p->data.authors, FIO) > -1){
			cout << "\t���� \t\t\t" << p->data.code << endl;
			cout << "\t������: \t\t" << p->data.authors << endl;
			cout << "\t��������: \t\t" << p->data.name << endl;
			cout << "\t������������: \t\t" << p->data.publisher << endl;
			cout << "\t��� �������:\t\t" << p->data.year << endl;
			cout << "\t����������:\t\t" << p->data.count << endl;
			cout << "\t�������:\t\t" << p->data.available << endl;
		}
		searchBookFIO(p->left, FIO);
		searchBookFIO(p->right, FIO);
	}
}

int searchBookInOut(node *p, char code[9], bool InOut);
//���������� � ����-������
void insertNode(inout k){
	int i = searchBookInOut(ABLTreeRoot, k.code, true);
	if (i >= 0){
		Node *p = new Node;
		p->next = NULL;
		strcpy(p->data.code, k.code);
		strcpy(p->data.number, k.number);
		strcpy(p->data.date1, k.date1);
		strcpy(p->data.date2, k.date2);
		if (skiplist == NULL){
			skiplist = p;
			last = p;
			return;
		}
		last->next = p;
		last = p;
	}
	else
		cout << "��� ������ ����� ����";
}

//������� �����
void editNode(inout k){
	int i = searchBookInOut(ABLTreeRoot, k.code, false);
	Node *p = new Node;
	p = skiplist;
	while (1){
		if (strcmp(p->data.code, k.code) == 0 &&
			strcmp(p->data.number, k.number) == 0 &&
			strcmp(p->data.date1, k.date1) == 0 &&
			strcmp(p->data.date2, "empty") == 0){
			strcpy(p->data.date2, k.date2);
		}
		if (p->next == NULL) return;
		p = p->next;
	}
}

//����� �� ��
void searchInOutNumber(char number[10]){
	Node *p = new Node;
	p = skiplist;
	while (1){
		if (DirectSearch(p->data.number, number) > -1){
			cout << "\t����:\t\t\t" << p->data.code << endl;
			cout << "\t���:\t\t\t" << p->data.number << endl;
			cout << "\t������: \t\t" << p->data.date1 << endl;
			cout << "\t�������:\t\t" << p->data.date2 << endl;
		}
		if (p->next == NULL) return;
		p = p->next;
	}
}

//����� �� �����
void searchInOutCode(char code[9]){
	Node *p = new Node;
	p = skiplist;
	while (1){
		if (strcmp(p->data.code, code) == 0){
			cout << "\t����:\t\t\t" << p->data.code << endl;
			cout << "\t���:\t\t\t" << p->data.number << endl;
			cout << "\t������: \t\t" << p->data.date1 << endl;
			cout << "\t�������:\t\t" << p->data.date2 << endl;
		}
		if (p->next == NULL) return;
		p = p->next;
	}
}

//����� �� ������ ��
void serchReaderNumber(void){
	char temp[10];
	cout << "������� ����� ������������� ������: ";
	do{
		cin.clear();
		cin.sync();
		cin.getline(temp, sizeof(temp));
		cout << endl;
		Rus_1251(temp, temp);
	} while (isAccess(temp[0]) || isNumber(temp[1]) ||
		isNumber(temp[2]) || isNumber(temp[3]) ||
		isNumber(temp[4]) || isDash(temp[5]) ||
		isNumber(temp[6]) || isNumber(temp[7]));
	for (int i = 0; i < maxr; i++)
		if (HashTableUsed[i] && DirectSearch(HashTable[i].number, temp) > -1){
		cout << "������: " << i << endl;
		cout << "\t���\t\t" << HashTable[i].number << endl;
		cout << "\t���\t\t" << HashTable[i].FIO << endl;
		cout << "\t�����\t\t" << HashTable[i].adress << endl;
		cout << "\t��� ����.\t" << HashTable[i].year << endl;
		cout << "\t����� �/�\t" << HashTable[i].place << endl;
		searchInOutNumber(HashTable[i].number);
		return;
		}
	system("PAUSE");
}

//����� ����� �� �����
void searchBookNumber(node *p, char code[9]){
	if (p != NULL){
		if (strcmp(p->data.code, code) == 0){
			cout << "\t���� \t\t\t" << p->data.code << endl;
			cout << "\t������: \t\t" << p->data.authors << endl;
			cout << "\t��������: \t\t" << p->data.name << endl;
			cout << "\t������������: \t\t" << p->data.publisher << endl;
			cout << "\t��� �������:\t\t" << p->data.year << endl;
			cout << "\t����������:\t\t" << p->data.count << endl;
			cout << "\t�������:\t\t" << p->data.available << endl;
			searchInOutCode(code);
		}
		searchBookFIO(p->left, code);
		searchBookFIO(p->right, code);
	}
}

int searchBookInOut(node *p, char code[9], bool InOut){
	if (p != NULL){
		if (strcmp(p->data.code, code) == 0){
			if (InOut) p->data.available--;
			else p->data.available++;
			return p->data.available;
		}
		searchBookInOut(p->left, code, InOut);
		searchBookInOut(p->right, code, InOut);
	}
	return -1;
}
//################################################################################
//############### ������� ������������� ���������� ������� #######################
//################################################################################

void SetDefaultReader(void){
	char	number[10][10] = {//����� ������������� ������
			{ "�0001-12" },
			{ "�0002-12" },
			{ "�0003-12" },
			{ "�0004-13" },
			{ "�0005-13" },
			{ "�0006-13" },
			{ "�0007-14" },
			{ "�0008-14" },
			{ "�0009-14" },
			{ "�0010-14" }
	};

	char	FIO[10][150] = {//���
			{ "������ ���� ��������" },
			{ "������ ���� ��������" },
			{ "������� ����� ���������" },
			{ "�������� ������� ����������" },
			{ "�������� ������ ���������" },
			{ "������� ����� ��������" },
			{ "������� ���� ��������" },
			{ "��������� ������� ���������" },
			{ "������� ������� ��������" },
			{ "�������� ���� ����������" }
	};

	int		year[10] = {	//��� ��������
		1998, 1998, 1997, 1985, 1986,
		1985, 1980, 1987, 1985, 1990
	};

	char	adress[10][255] = {	//�����
			{ "�. ������ ��. ������ 1 11" },
			{ "�. ������ ��. ������ 2 12" },
			{ "�. ������ ��. ������ 3 13" },
			{ "�. ������ ��. ������ 4 14" },
			{ "�. ������ ��. ������ 5 15" },
			{ "�. ������ ��. ������ 6 16" },
			{ "�. ������ ��. ������ 7 17" },
			{ "�. ������ ��. ������ 2 12" },
			{ "�. ������ ��. ������ 2 12" },
			{ "�. ������ ��. ������ 2 12" }
	};
	char	place[10][255] = {	//����� ������/�����
			{ "��� ���� � ������" },
			{ "��� ������ � ����" },
			{ "��� ������� � ���������" },
			{ "���������� �������� ��� �������� (����)" },
			{ "������������ �������" },
			{ "��� ���� � ������" },
			{ "��� ������ � ����" },
			{ "��� ������� � ���������" },
			{ "����������� �������� ��� �������� (����)" },
			{ "������������ �������" },
	};

	reader tempReader;

	for (int i = 0; i < 10; i++){
		strcpy(tempReader.number, number[i]);
		strcpy(tempReader.FIO, FIO[i]);
		strcpy(tempReader.adress, adress[i]);
		strcpy(tempReader.place, place[i]);
		tempReader.year = year[i];

		if (insertDataToHash(tempReader)) cout << "�������� �������� � ������� " << i << " �� ������ ��-���������" << endl;
		else cout << "������ ��� ���������� � ���-�������" << endl;
	}
}

void SetDefaultBook(void){
	char	code[10][9] = {			//����
			{ "000.001" },
			{ "000.002" },
			{ "000.003" },
			{ "111.001" },
			{ "111.002" },
			{ "111.003" },
			{ "222.111" },
			{ "222.111" },
			{ "222.333" },
			{ "222.444" }
	};
	char	authors[10][255] = {	//�����(�)
			{ "������ �. �. ������ �. �. ������� �. �." },
			{ "������ �. �." },
			{ "������ �. �." },
			{ "������� �. �." },
			{ "������ �. �. ������ �. �." },
			{ "������ �. �. ������� �. �." },
			{ "������ �. �. ������� �. �." },
			{ "�������� �. �." },
			{ "��������� �. �." },
			{ "����� �. �." }
	};
	char	name[10][255] = {		//��������
			{ "��������� � ��������� ������. ��� 1." },
			{ "��������� � ��������� ������. ��� 2." },
			{ "��������� � ��������� ������. ��� 3." },
			{ "��������� � ��������� ������. ��� 4." },
			{ "���������. ��� 1." },
			{ "���������. ��� 2." },
			{ "���������. ��� 3." },
			{ "��������� ������. ��� 1." },
			{ "��������� ������. ��� 2." },
			{ "��������� ������. ��� 3." }
	};
	char	publisher[10][255] = {	//������������
			{ "������" },
			{ "������" },
			{ "������" },
			{ "������" },
			{ "������" },
			{ "������" },
			{ "������" },
			{ "������" },
			{ "������" },
			{ "������" }
	};
	int		year[10] = {		//��� �������
		1999, 1999, 1999, 2000, 2000,
		1999, 1999, 1999, 2000, 2000
	};

	int		count[10] = {		//���������� �����������
		10, 10, 10, 10, 10,
		10, 10, 10, 10, 10
	};

	int		available[10] = {	//���������� � �������
		10, 10, 10, 10, 10,
		10, 10, 10, 10, 10
	};
	book temp;
	for (int i = 0; i < 10; i++){
		strcpy(temp.authors, authors[i]);
		temp.available = available[i];
		strcpy(temp.code, code[i]);
		temp.count = count[i];
		strcpy(temp.name, name[i]);
		strcpy(temp.publisher, publisher[i]);
		temp.year = year[i];
		ABLTreeRoot = insert(ABLTreeRoot, temp);
		if (ABLTreeRoot) cout << "��������� ����� � ������� " << i << " �� ������ ��-���������" << endl;
		else cout << "������ ���������� � ���-������";
	};
}

void SetDefaultInout(void){
	char	code[10][9] = {			//����
			{ "000.001" },
			{ "000.002" },
			{ "000.003" },
			{ "111.001" },
			{ "111.002" },
			{ "111.003" },
			{ "222.111" },
			{ "222.111" },
			{ "222.333" },
			{ "222.444" }
	};

	char	number[10][10] = {	//����� ������������� ������
			{ "�0001-12" },
			{ "�0002-12" },
			{ "�0003-12" },
			{ "�0004-13" },
			{ "�0005-13" },
			{ "�0006-13" },
			{ "�0007-14" },
			{ "�0008-14" },
			{ "�0009-14" },
			{ "�0010-14" }
	};
	char	date1[10][12] = {			//���� ������
			{ "10.11.2013" },
			{ "10.11.2013" },
			{ "10.11.2013" },
			{ "10.11.2013" },
			{ "10.11.2013" },
			{ "10.11.2013" },
			{ "10.11.2013" },
			{ "10.11.2013" },
			{ "10.11.2013" },
			{ "10.11.2013" }
	};
	char	date2[10][12] = {			//���� ��������
			{ "10.12.2013" },
			{ "10.12.2013" },
			{ "10.12.2013" },
			{ "10.12.2013" },
			{ "10.12.2013" },
			{ "10.12.2013" },
			{ "10.12.2013" },
			{ "10.12.2013" },
			{ "empty" },
			{ "empty" }
	};
	inout temp;
	for (int i = 0; i < 10; i++){
		strcpy(temp.code, code[i]);
		strcpy(temp.number, number[i]);
		strcpy(temp.date1, date1[i]);
		strcpy(temp.date2, date2[i]);
		insertNode(temp);
		cout << "��������� ������ � ������� " << i << " �� ������ ��-���������" << endl;
	};
}

//############################################################################
//############################ ������� ���� ##################################
//############################################################################
bool main_menu()
{
	system("cls");
	std::cout << "�������� ����� ����: " << std::endl
		<< "\t1.\t ����������� ������ ��������" << std::endl
		<< "\t2.\t ������ �������� � ������������" << std::endl
		<< "\t3.\t �������� ���� ������������������ ���������" << std::endl
		<< "\t4.\t ������� ������ � ���������" << std::endl
		<< "\t5.\t ����� �������� �� ���" << std::endl
		<< "\t6.\t ����� �������� �� ���" << std::endl
		<< "\t7.\t ���������� ����� �����" << std::endl
		<< "\t8.\t �������� �������� � �����" << std::endl
		<< "\t9.\t �������� ���� ��������� ����" << std::endl
		<< "\t10.\t ������� ������ � ������" << std::endl
		<< "\t11.\t ����� ����� �� �����" << std::endl
		<< "\t12.\t ����� ����� �� ��� ������" << std::endl
		<< "\t13.\t ������ �����" << std::endl
		<< "\t14.\t ������� �����" << std::endl
		<< "\t15.\t �����" << std::endl;

	int selected_case;
	int counter;
	short i = 0, j = 0;

	cin >> selected_case;

	if (selected_case < 1 || selected_case > 15){
		std::cerr << "������ ������ ������ ����" << std::endl;
		system("pause");
		return true;
	}
	switch (selected_case)
	{
	case 1: // ����������� ������ ��������
		insertDataToHash(readerInput());
		break;
	case 2: // ������ �������� � ������������
		reader tempReader;
		cout << "������� ����� ������������� ������: ";
		cin.clear();
		cin.sync();
		do{
			cin.getline(tempReader.number, sizeof(tempReader.number));
			cout << endl;
			Rus_1251(tempReader.number, tempReader.number);
		} while (isAccess(tempReader.number[0]) || isNumber(tempReader.number[1]) ||
			isNumber(tempReader.number[2]) || isNumber(tempReader.number[3]) ||
			isNumber(tempReader.number[4]) || isDash(tempReader.number[5]) ||
			isNumber(tempReader.number[6]) || isNumber(tempReader.number[7]));
		if (deleteDataFromHash(tempReader)) cout << "�������� ���������" << endl;
		else cout << "������ �� �� �������" << endl;
		system("PAUSE");
		break;
	case 3: // �������� ���� ������������������ ���������
		showHashTable();
		break;
	case 4: // ������� ������ � ���������
		clearHashTable();
		break;
	case 5: // ����� �������� �� ���
		serchReaderNumber();
		system("PAUSE");
		break;
	case 6: // ����� �������� �� ���
		searchReaderFIO();
		break;
	case 7: // ���������� ����� �����
		ABLTreeRoot = insert(ABLTreeRoot, bookInput());
		break;
	case 8: // �������� �������� � �����
		book tempBook;
		cout << "������� ���� �����: ";
		do{
			cin.clear();
			cin.sync();
			cin.getline(tempBook.code, sizeof(tempBook.code));
			cout << endl;
			Rus_1251(tempBook.code, tempBook.code);
		} while (isNumber(tempBook.code[0]) || isNumber(tempBook.code[1]) ||
			isNumber(tempBook.code[2]) || isDot(tempBook.code[3]) ||
			isNumber(tempBook.code[4]) || isNumber(tempBook.code[5]) || isNumber(tempBook.code[6]));
		ABLTreeRoot = remove(ABLTreeRoot, tempBook);
		system("PAUSE");
		break;
	case 9: // �������� ���� ��������� ����
		preorder(ABLTreeRoot);
		system("PAUSE");
		break;
	case 10: // ������� ������ � ������
		emptyABLTree(ABLTreeRoot);
		break;
	case 11: // ����� ����� �� �����
		char tempCode[9];
		cout << "������� ���� �����: ";
		do{
			cin.clear();
			cin.sync();
			cin.getline(tempCode, sizeof(tempCode));
			cout << endl;
			Rus_1251(tempCode, tempCode);
		} while (isNumber(tempCode[0]) || isNumber(tempCode[1]) ||
			isNumber(tempCode[2]) || isDot(tempCode[3]) ||
			isNumber(tempCode[4]) || isNumber(tempCode[5]) || isNumber(tempCode[6]));
		searchBookNumber(ABLTreeRoot, tempCode);
		system("PAUSE");
		break;
	case 12: // ����� ����� �� ��� ������
		char searchFIO[255];
		system("CLS");
		cout << "������� ���: " << endl;
		cin.clear();
		cin.sync();
		cin.getline(searchFIO, sizeof(searchFIO));
		Rus_1251(searchFIO, searchFIO);
		searchBookFIO(ABLTreeRoot, searchFIO);
		system("PAUSE");
		break;
	case 13: //������ �����
		char tmpNumber[10];
		cout << "������� ����� ������������� ������: ";
		cin.clear();
		cin.sync();
		do{
			cin.getline(tmpNumber, sizeof(tmpNumber));
			cout << endl;
			Rus_1251(tmpNumber, tmpNumber);
		} while (isAccess(tmpNumber[0]) || isNumber(tmpNumber[1]) ||
			isNumber(tmpNumber[2]) || isNumber(tmpNumber[3]) ||
			isNumber(tmpNumber[4]) || isDash(tmpNumber[5]) ||
			isNumber(tmpNumber[6]) || isNumber(tmpNumber[7]));

		char tmpCode[9];
		cout << "������� ���� �����: ";
		do{
			cin.clear();
			cin.sync();
			cin.getline(tmpCode, sizeof(tmpCode));
			cout << endl;
			Rus_1251(tmpCode, tmpCode);
		} while (isNumber(tmpCode[0]) || isNumber(tmpCode[1]) ||
			isNumber(tmpCode[2]) || isDot(tmpCode[3]) ||
			isNumber(tmpCode[4]) || isNumber(tmpCode[5]) || isNumber(tmpCode[6]));
		char tmpDate[12];
		cout << endl << "������� ���� ������: ";
		cin.clear();
		cin.sync();
		cin.getline(tmpDate, sizeof(tmpDate));
		cout << endl;
		Rus_1251(tmpDate, tmpDate);
		inout tmp;
		strcpy(tmp.code, tmpCode);
		strcpy(tmp.number, tmpNumber);
		strcpy(tmp.date1, tmpDate);
		strcpy(tmp.date2, "empty");
		insertNode(tmp);
		break;
	case 14: //������� �����
		char tmNumber[10];
		cout << "������� ����� ������������� ������: ";
		cin.clear();
		cin.sync();
		do{
			cin.getline(tmNumber, sizeof(tmNumber));
			cout << endl;
			Rus_1251(tmNumber, tmNumber);
		} while (isAccess(tmNumber[0]) || isNumber(tmNumber[1]) ||
			isNumber(tmNumber[2]) || isNumber(tmNumber[3]) ||
			isNumber(tmNumber[4]) || isDash(tmNumber[5]) ||
			isNumber(tmNumber[6]) || isNumber(tmNumber[7]));

		char tmCode[9];
		cout << "������� ���� �����: ";
		do{
			cin.clear();
			cin.sync();
			cin.getline(tmCode, sizeof(tmCode));
			cout << endl;
			Rus_1251(tmCode, tmCode);
		} while (isNumber(tmCode[0]) || isNumber(tmCode[1]) ||
			isNumber(tmCode[2]) || isDot(tmCode[3]) ||
			isNumber(tmCode[4]) || isNumber(tmCode[5]) || isNumber(tmCode[6]));
		char tmDate1[12], tmDate2[12];
		cout << endl << "������� ���� ������: ";
		cin.clear();
		cin.sync();
		cin.getline(tmDate1, sizeof(tmDate1));
		cout << endl;
		Rus_1251(tmDate1, tmDate1);
		cout << endl << "������� ���� ��������: ";
		cin.clear();
		cin.sync();
		cin.getline(tmDate2, sizeof(tmDate2));
		cout << endl;
		Rus_1251(tmDate2, tmDate2);

		inout tm;
		strcpy(tm.code, tmCode);
		strcpy(tm.number, tmNumber);
		strcpy(tm.date1, tmDate1);
		strcpy(tm.date2, tmDate1);
		editNode(tm);
		break;
	case 15: //�����
		return false;
		break;
	}
	return true;
}


//############################################################################
//###################### ������� ������� ��������� ###########################
//############################################################################
int main(int argc, _TCHAR* argv[]){
	setlocale(LC_ALL, "Russian");
	system("CLS");

	//��������� ������ ��-���������
	SetDefaultReader();
	SetDefaultBook();
	SetDefaultInout();
	while (main_menu())
		cout << endl;
	system("PAUSE");
	return 0;
}
